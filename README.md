Instrucciones 
1- Crear los contenedores bastion, Sales, Accounting y  HR.
2- Actualizar e instalar python, nano y ssh en los contenedores.
3- Agregar el authorized key en los contenedores.
4- En mi caso cree una Network de docker en donde agregue todos los contenedores.
5- Agregar los dns de los contenedores en el archivo hosts del bastion.
6- Crear el archivo playbook.yml con las Instrucciones.
7- Ejecutar el comando ansibleplaybook
